// click on a ruleset name to see its source here
ruleset wovyn_base {
  meta {
    name "Wovyn Base"
    use module sensor_profile alias profile
    use module io.picolabs.lesson_keys
    use module io.picolabs.twilio_v2 alias twilio
        with account_sid = keys:twilio{"account_sid"}
             auth_token =  keys:twilio{"auth_token"}

    description <<Base for the first lab with the Wovyn sensor>>
    author "Jon Meng"
    logging on
    shares hello
    shares __testing
  }
  
  rule process_heartbeat {
    select when wovyn heartbeat where genericThing

    pre {
      temperatureF = event:attr("genericThing").values(["data", "temperature"]).head()["temperatureF"].klog("Attributes Recieved (temperatureF): ")
      timestamp = time:now() // Formatted timestamp   
    }
    always {
      raise wovyn event "new_temperature_reading"
         attributes { "temperature": temperatureF, "timestamp": timestamp }
    }
  }

  rule find_high_temps {
    select when wovyn new_temperature_reading

    pre {
       temperature = event:attr("temperature").klog("Find_high_temps: ")
       temperature_threshold = profile:threshold().as("Number");   
       violation = temperature > temperature_threshold
       message = violation => "Temperature Violation!" | "NO Temperature Violation."
    }

    send_directive("say", {"something": message});

    always {
        raise wovyn event "threshold_violation"
           attributes event:attrs()
	if (violation);
    }	
  }

  rule threshold_notification {
    select when wovyn threshold_violation	

    pre {
       temperature = event:attr("temperature")
    }

    twilio:send_sms(profile:contact_number(),
                    "+12674940026",
                    "Temperature Violation! Temperature: "+temperature + "F") 
  }
}
