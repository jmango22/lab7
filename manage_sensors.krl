ruleset manage_sensors {
    meta {
        use module io.picolabs.wrangler alias wrangler
    }

    global {
        nameFromID = function(section_id) {
            section_id
        };

        nameExists = function(name){
            ent:sensors.defaultsTo({}) >< name
        };

	allTemperatures = function() {
            // map returns a new map with each key mapped to the result of the function
            ent:sensors.map(function(v,k){
              wrangler:skyQuery(v{"eci"},"temperature_store","temperatures");
            });
        };

        default_threshold = "70"
    }

    // testing ruleset, returns the ent:sensors data structure
    rule sensors {
        select when sensor get_sensors

        send_directive("sensors", {"sensors": ent:sensors})
    }

    // test ruleset, clears the data structure with the sensors
    rule clear_sensors {
        select when sensor clear_sensors

        always {
            ent:sensors := {}
        }
    }

    rule test_temperatures {
        select when sensor temps

	pre {
	    temps = allTemperatures()
	}

        send_directive("Temperatures", {"Temps": temps})
    }

    rule section_already_exists {
        select when sensor new_sensor
        pre {
            section_id = event:attr("section_id")
            exists = ent:sensors >< section_id
        }
        if exists then
            send_directive("section_ready", {"section_id": section_id})
    }

    rule section_needed {
        select when sensor new_sensor
        pre {
            section_id = event:attr("section_id")
            exists = ent:sensors >< section_id
        }
        if not exists
        then
            noop()
        fired {
            raise wrangler event "child_creation"
            attributes { "name": nameFromID(section_id),
                        "color": "#ffff00",
                        "section_id": section_id,
                        "rids": ["temperature_store", "wovyn_base", "sensor_profile"] }
        }
    }

    rule store_new_section {
        select when wrangler child_initialized
        pre {
            the_section = {"id": event:attr("id"), "eci": event:attr("eci")}
            section_id = event:attr("rs_attrs"){"section_id"}
            eci = event:attr("eci")
        }
        if section_id.klog("found section_id")
        then
            noop()
        fired {
            ent:sensors := ent:sensors.defaultsTo({});
            ent:sensors{[section_id]} := the_section;
            
            raise sensor event "update_profile"
                attributes {"section_id": section_id, "eci": eci}
        }
    }
    
    rule update_child_profile {
        select when sensor update_profile
        pre {
            section_id = event:attr("section_id")
            eci = event:attr("eci")
            exists = ent:sensors >< section_id
        }
            
        // Programmatically send a sensor:profile_updated event to the child after it's created to set its name,
        // notification SMS number, and default threshold.
        if exists then    
            event:send({ 
                "eci": eci, 
                "eid": "profile",
                "domain": "sensor", 
                "type": "profile_updated",
                "attrs": { "name": section_id, "number": "+15098201634", "threshold": default_threshold } 
            })
    }
    
    rule delete_sensor {
        select when sensor unneeded_sensor
        
        pre {
            section_id = event:attr("section_id")
            exists = ent:sensors >< section_id
            child_to_delete = nameFromID(section_id)
        }

        if exists then
            send_directive("deleting_section", {"section_id":section_id})
        fired {
            raise wrangler event "child_deletion"
                attributes {"name": child_to_delete};
            clear ent:sensors{[section_id]}
        }
    }
}
