ruleset sensor_profile {
  meta {
     provides name, location, contact_number, threshold 
     shares name, location, contact_number, threshold 
  }

  global {
     name = function() {
         ent:name.defaultsTo("Meng Sensor");
     };

     location = function() {
         ent:location.defaultsTo("Meng Appartment");
     };
     
     contact_number = function() {
         ent:contact_number.defaultsTo("+15098201634");
     };

     threshold = function() {
         ent:threshold.defaultsTo("70");
     };
  }
  
  rule sensor_profile {
     select when sensor profile_updated

     pre {
         name = event:attr("name").defaultsTo("Meng Sensor")
         location = event:attr("location").defaultsTo("Meng Appartment")
         contact_number = event:attr("number").defaultsTo("+15098201634")
         threshold = event:attr("threshold").defaultsTo("70")
     }

     send_directive("say", {"message": "New ruleset working!"});

     always {
         ent:name := name;
         ent:location := location;
         ent:contact_number := contact_number;
         ent:threshold := threshold 
    }
  }

  rule get_sensor_profile {
      select when sensor profile

      send_directive("say", {"name": ent:name, "location": ent:location, "contact_number": ent:contact_number, "threshold": ent:threshold}); 
  }
}
